# Helm Charts
This repository collects newsroom.bi not official charts from their individual repos and automatically publish them to our Helm repo, located at [helm.newsroom.bi](https://helm.newsroom.bi/index.yaml).

The charts collected and published currently by this repository are the following:

| Chart name   | Status       | Remark
|--------------|--------------|----------|
| [auto-deploy-app](https://gitlab.com/newsroom.bi/auto-deploy-app) | Forked | Adds functionality to GitLab's auto-deploy-app. |
| [helm-cronjobs](https://gitlab.com/newsroom.bi/helm-cronjobs) | Forked | Adds functionality to add kubernetes CronJobs. |
